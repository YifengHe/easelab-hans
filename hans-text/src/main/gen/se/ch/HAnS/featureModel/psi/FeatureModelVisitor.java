// This is a generated file. Not intended for manual editing.
package se.ch.HAnS.featureModel.psi;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;

public class FeatureModelVisitor extends PsiElementVisitor {

  public void visitFeature(@NotNull FeatureModelFeature o) {
    visitPsiElement(o);
  }

  public void visitProjectName(@NotNull FeatureModelProjectName o) {
    visitPsiElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}
