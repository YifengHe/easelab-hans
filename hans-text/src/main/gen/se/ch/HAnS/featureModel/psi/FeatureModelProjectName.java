// This is a generated file. Not intended for manual editing.
package se.ch.HAnS.featureModel.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface FeatureModelProjectName extends PsiElement {

  String renameFeature();

  String addFeature();

  int deleteFeature();

  //WARNING: addMiscFeature(...) is skipped
  //matching addMiscFeature(FeatureModelProjectName, ...)
  //methods are not found in FeatureModelPsiImplUtil

  //WARNING: getFeatureName(...) is skipped
  //matching getFeatureName(FeatureModelProjectName, ...)
  //methods are not found in FeatureModelPsiImplUtil

}
