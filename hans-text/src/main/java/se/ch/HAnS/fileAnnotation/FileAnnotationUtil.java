package se.ch.HAnS.fileAnnotation;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.gson.Gson;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiComment;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.util.PsiTreeUtil;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import se.ch.HAnS.fileAnnotation.psi.FileAnnotationFile;
import se.ch.HAnS.fileAnnotation.psi.FileAnnotationFileReferences;
import se.ch.HAnS.fileAnnotation.psi.FileAnnotationLpq;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class FileAnnotationUtil {

    /**
     * Searches the entire project for FeatureModel language files with instances of the Simple property with the given key.
     *
     * @param project current project
     * @param featurename     to check
     * @return matching properties
     */
    public static List<FileAnnotationLpq> findFileLPQs(Project project, String featurename) {
        List<FileAnnotationLpq> result = new ArrayList<>();
        Collection<VirtualFile> virtualFiles =
                FileTypeIndex.getFiles(FileAnnotationFileType.INSTANCE, GlobalSearchScope.allScope(project));
        for (VirtualFile virtualFile : virtualFiles) {
            FileAnnotationFile fileAnnotationFile = (FileAnnotationFile) PsiManager.getInstance(project).findFile(virtualFile);
            if (fileAnnotationFile != null) {
                FileAnnotationLpq[] properties = PsiTreeUtil.getChildrenOfType(fileAnnotationFile, FileAnnotationLpq.class);
                if (properties != null) {
                    for (FileAnnotationLpq property : properties) {
                        if (featurename.equals(property.getText())) {
                            result.add(property);
                        }
                    }
                }
            }
        }
        return result;
    }

    public static Map<String, String> featuretofile(Project project) {
        Map<String, String> map = new HashMap<>();
        String[] files = null;
        boolean exist = false;
        String[] children;
        Collection<VirtualFile> virtualFiles =
                FileTypeIndex.getFiles(FileAnnotationFileType.INSTANCE, GlobalSearchScope.allScope(project));
        for (VirtualFile virtualFile : virtualFiles) {
            @Nullable PsiFile folderAnnotationFileFile = PsiManager.getInstance(project).findFile(virtualFile);
            String text = folderAnnotationFileFile.getText();
            String[] lines = text.split("\n");
            for (String line : lines) {
                //System.out.println("Line is here: " + line);
                if (line.isEmpty()) {
                    continue;
                }
                if (exist == false) {
                    files = line.split("," + "\\s+");
                    exist = true;
                    continue;
                } else {
                    children = line.split("," + "\\s+");
                    for (String child : children) {
                        for (String file : files) {
                            map.put(child, file);
                        }
                    }
                    exist = false;
                }
            }
        }
        return map;
    }


    public static List<FileAnnotationLpq> findFileLPQs(Project project) {
        List<FileAnnotationLpq> result = new ArrayList<>();
        //
        Map<String, String> map = new HashMap<>();
        //
        Collection<VirtualFile> virtualFiles =
                FileTypeIndex.getFiles(FileAnnotationFileType.INSTANCE, GlobalSearchScope.allScope(project));
        for (VirtualFile virtualFile : virtualFiles) {
            @Nullable PsiFile folderAnnotationFileFile = PsiManager.getInstance(project).findFile(virtualFile);
            if (folderAnnotationFileFile != null) {
                FileAnnotationLpq[] properties = PsiTreeUtil.getChildrenOfType(folderAnnotationFileFile, FileAnnotationLpq.class);
                if (properties != null) {
                    Collections.addAll(result, properties);
                }
            }
        }
        return result;
    }


    public static Map<FileAnnotationLpq, FileAnnotationFileReferences> findAllFileMappings(Project project) {
        Map<FileAnnotationLpq, FileAnnotationFileReferences> featureFileMap = new HashMap<>();
        List<FileAnnotationLpq> fileAnnotationFeatureList = findFileLPQs(project);

        for (FileAnnotationLpq feature: fileAnnotationFeatureList ) {
            if (feature.getParent().getPrevSibling() instanceof FileAnnotationFileReferences ) {
                featureFileMap.put(feature, (FileAnnotationFileReferences)feature.getParent().getPrevSibling());
            }
            //System.out.println("qq" + feature.getText());//
        }
        return featureFileMap;
    }

    public static Map<FileAnnotationLpq, FileAnnotationFileReferences> findFileMapping(Project project, String featureName) {
        Map<FileAnnotationLpq, FileAnnotationFileReferences> featureFileMap = new HashMap<>();
        List<FileAnnotationLpq> fileAnnotationFeatureList = findFileLPQs(project, featureName);

        for (FileAnnotationLpq feature: fileAnnotationFeatureList ) {
            if (feature.getParent().getPrevSibling() instanceof FileAnnotationFileReferences ) {
                featureFileMap.put(feature, (FileAnnotationFileReferences)feature.getParent().getPrevSibling());
            }
        }

        return featureFileMap;
    }

}
