// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

var tangleType = ["Line tangle: ", "Block tangle: ", "File tangle: ", "Folder tangle: "]
var nodes = new vis.DataSet;

var edges = new vis.DataSet;

var network;

var oncontextFeature;

//Listener for when tangling data is saved on x, we create the graph
x = {
    aInternal: "Hej",
    aListener: function(val) {},
    set a(val) {
        this.aInternal = val;
        this.aListener(val);
    },
    get a() {
        return this.aInternal;
    },
    registerListener: function(listener) {
        this.aListener = listener;
    }
}
//Below is code fired when x is changed (or in this case when tangling data is saved on x)
x.registerListener(function(val) {
    const obj = JSON.parse(x.a)
    getNodes(obj)
    getEdges()
    renderAll()
    initListeners()
});

function saveData(response){
    x.a = response;
}

sendData2("OpenTangle",saveData, sendDataFailure);



// Using methods from vis.js DataSet inside function
function getNodes(jsonData) {
    var tempnode;

    //Add the selected feature first
    nodes.update({"id": Object.keys(jsonData)[0], "label": Object.keys(jsonData)[0], "color": "#e24343"})
    //Loop through the different tangling types
    for (var i = 0; i < Object.values(jsonData)[0].length; i++) {
        var tangleName = tangleType[i];
        //Loop through the tangled features
        for(var j = 0; j < Object.values(jsonData)[0][i].length; j++) {
            //Save the feature and it's degree of tangling
            tempnode = Object.values(jsonData)[0][i][j];
            //Add feature if it does not exist
            if(nodes.get(tempnode[0]) == undefined) {
                nodes.add({
                    "id": tempnode[0],
                    "label": tempnode[0],
                    "group": "other",
                    "title": "",
                    "tangle": 0 });
                // Add tangle data in nodes, "tangle" is used for the edges, could also be used for the hover box
                //  along with [tangleName]
                // Add hover text for a tangle type, maybe not needed if we have the hover box
                nodes.updateOnly({
                    "id": tempnode[0],
                    "title": tangleName+tempnode[1],
                    "tangle": parseInt(tempnode[1]),
                    [tangleName]:  parseInt(tempnode[1])
                 })
            } else {
                // Add next tangle data in nodes
                // Add next hover text for tangle type, maybe not needed if we have the hover box
                nodes.updateOnly({
                    "id": tempnode[0],
                    "title": nodes.get(tempnode[0]).title+"\n"+tangleName+tempnode[1] ,
                    "tangle": nodes.get(tempnode[0]).tangle+parseInt(tempnode[1]),
                    [tangleName]:  parseInt(tempnode[1])
                })
            }
        }
    }

}

function getEdges(){

    // Save selected feature
    var focused = nodes.getIds()[0];
    // Loop through all "non-selected" nodes/feature, feature needs to be tangled for it to work
    for(var i = 1; i < nodes.getIds().length; i++){
        var tempnode = nodes.getIds()[i]
        // Add the edges and the combined tangling value for edge width
        edges.add({"from": focused, "to": tempnode , "value": nodes.get(tempnode).tangle })
    }
}

function renderAll(){
    var container = document.getElementById("tangleView");
    var data = {
        nodes: nodes,
        edges: edges,
    };
    var options = {
        edges: {
            physics: false,
            smooth: {
                enabled: false,
                type: "cubicBezier",
                roundness: 0.5,
            },
            color: {
                color: "#de2e1f",
                highlight: "#db9c92"
            },
            scaling: {
                min: 3,
                max: 22
            }
        },
        nodes: {
            shape: "dot",
            borderWidth: 2,
            font: {
                color: "white",
            },
        },
        layout: {
            randomSeed: 2
        },
        groups: {
            other: {
                color: "#de823b", // alla i gruppen other får denna färgen
            },
        },
        /*
        configure: {
            enabled: true,
        },
        */

    };
    network = new vis.Network(container, data, options);
}
/*
// create a network
var container = document.getElementById("tangleView");
var data = {
    nodes: nodes,
    edges: edges,
};
var options = {
    edges: {
        physics: false,
        smooth: {
            enabled: false,
            type: "cubicBezier",
            roundness: 0.5,
        },
        color: {
            color: "#de2e1f",
            highlight: "#db9c92"
        },
        scaling: {
            min: 3,
            max: 22
        }
    },
    nodes: {
        shape: "dot",
        borderWidth: 2,
        font: {
            color: "white",
        },
    },
    layout: {
        randomSeed: 2
    },
    groups: {
        other: {
                color: "#de823b", // alla i gruppen other får denna färgen
        },
    },


};
*/

//var network = new vis.Network(container, data, options);

network.storePositions()


function initListeners() {

//  Our own context menu
    network.on("oncontext", function (params) {
        var contextElement = document.getElementById("context-menu");

        // Prevent default context menu showing up
        params.event.preventDefault();

        //  Save the node we right-click on, if not it will be "undefined"
        oncontextFeature = this.getNodeAt(params.pointer.DOM)

        // Check if it is a node that we right-click
        if (oncontextFeature) {

            //  Show that the node is selected
            this.selectNodes([oncontextFeature])

            //  Get position that context menu will popup
            contextElement.style.top = params.pointer.DOM.y + "px";
            contextElement.style.left = params.pointer.DOM.x + "px";

            //  Show the context menu
            contextElement.classList.add("active");

        } else {
            oncontextFeature = null;
            // Show that the node is not selected anymore
            this.unselectAll();

            //  Remove the context menu
            contextElement.classList.remove("active");
        }

        //  Just a console log to see if correct node is selected
        console.log("Node id=" + oncontextFeature)

    });

    document.addEventListener("click", function (e) {
        var contextElement = document.getElementById("context-menu");
        //  Get element that we clicked on
        var isContextMenuLink = clickInsideElement(e, "context-menu__link")

        //  Get the action name and do accordingly
        //  As of now, only console log what type of action
        if (isContextMenuLink) {

            //  Currently switch case but could use something else
            switch (getcontextAction(isContextMenuLink)) {
                case "featureLocation": // THIS IS: VIEW FEATURE TO FILE
                    sendData2("OpenLocation " + oncontextFeature,saveData, sendDataFailure);
                    break;
                default:
                    console.log("is this possible?")
            }
            //  Console log to show again what feature the context menu is about
            console.log("Do action on: " + oncontextFeature)

            //  Remove the context menu, probably should not be here, should be right after the not yet implemented actions
            contextElement.classList.remove("active");
        }
        //  Remove the context menu
        contextElement.classList.remove("active");
    });

    network.on("dragEnd", function (params) {
        network.storePositions()
    });
}

function clickInsideElement( e, className ) {
    //Get the event
    var el = e.target;

    // Make sure that on an element or that its parent has classname
    if ( el.classList.contains(className) ) {
        return el;
    } else {
        while ( el = el.parentNode ) {
            if ( el.classList && el.classList.contains(className) ) {
                return el;
            }
        }
    }
    return false;
}

//  Function to get the action of what we clicked on
function getcontextAction(link) {
    return link.getAttribute("data-action");
}
