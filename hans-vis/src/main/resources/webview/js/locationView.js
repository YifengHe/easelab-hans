let chartData;
let resBase64;
let selElement;
let reqHost;
let iconDir = "../images/question_mark.png";

function hideMenu() {
    document.getElementById("contextMenu")
        .style.display = "none"
}

function rightClick(param) {
    if (document.getElementById("contextMenu").style.display === "block") {
        hideMenu();
    } else {
        let e = param.event;
        let menu = document.getElementById("contextMenu");
        menu.style.display = "block";
        menu.style.left = e.offsetX + "px";
        menu.style.top = e.offsetY + 20 + "px";
    }
}

function refresh(){
    location.reload();
}

/**
 * adapt the response data (feature-to-folder) to specific JSONString which in format:
 {
        "name": "root",
        "children": [{
            "name": "sub-root",
            "children": [{
				"name": 'attribute1',
				"value": '1'
			},{
				"name": 'attribute2',
				"value": '1'
			},{
				"name": 'attribute3',
				"value": '1'
			},{
				"name": 'attribute4',
				"value": '1'
			}]
       	}]
    }
 * @param resData
 * @returns {{symbol: string, name: string, itemStyle: {color: string}, value: string}}
 */
function parsingData(resData, req) {
    let parsedData = JSON.parse(resData);
    let parentArr = [];
    let dataTree = {
        "name": "root",
        "value": '0',
        "symbol": "circle",
        "itemStyle": {
            color: "#d0061e"
        },
    };
    reqHost = req;
    if(reqHost === "FoMappingDataFromBackend"){
        iconDir = "../images/FolderIcon2.svg";
    }
    if(reqHost === "FiMappingDataFromBackend"){
        iconDir = "../images/FileIcon2.svg";
    }
    for (let k in parsedData) {
        let v = String(parsedData[k]).replace("[", "").replace("]", "");
        let childArr = [];
        let parentNode = {
            "name": k,
            "value": '1',
            "symbol": "circle",
            "itemStyle": {
                color: "#d00695"
            },
        };
        let childNode = {
            "name": v,
            "value": '2',
            "symbol": "circle",
            "label": {
                width: 100,
                height: 40,
                lineHeight: 40,
                position: 'right',
                verticalAlign: 'middle',
                align: 'center',
                rich: {
                    img1: {
                        backgroundColor: {
                            image: iconDir,
                        },
                        height: 40
                    }
                },
                formatter: function (param) {
                    let res = "";
                    res += '{img1|}' + param.name;
                    return res;
                }
            }
        }
        childArr.push(childNode);
        parentNode.children = childArr;
        parentArr.push(parentNode);
    }
    dataTree.children = parentArr;
    return dataTree;
}

/**
 * Print the current location view and then refresh the page
 */
function printImage() {
    $("#snapshot").attr("src", resBase64).show(); // canvas rendering by ajax
    setTimeout(function () {
        // partial printing
        bdhtml = window.document.body.innerHTML; // get the whole html page
        sprnstr = "<!--start-print-->"; // begin of section
        eprnstr = "<!--end-print-->"; // end of section
        prnhtml = bdhtml.substr(bdhtml.indexOf(sprnstr) + 18);
        prnhtml = prnhtml.substring(0, prnhtml.indexOf(eprnstr));

        window.document.body.innerHTML = prnhtml; // preview the printed section in html page
        window.print(); // start print
        location.reload(); // page refresh after printing
    }, 3500);
}

/**
 *  Add tree node.
 */
function addNode() {
    let selValue = selElement.value;
    let nodeName = "";
    let nodeFolderDir = "";
    let action = "";

    if (selValue !== "0") {
        alert("Please select root node if you want to add a new one.");
        return;
    }
    if(reqHost === "FoMappingDataFromBackend"){
        action = 'AddFo';
    }
    if(reqHost === "FiMappingDataFromBackend"){
        action = 'AddFi';
    }

    nodeName = prompt("Node name ?");
    nodeFolderDir = prompt("Please enter folder directory.");
    let options = chartData.getOption();
    let nodesOption = options.series[0].data;

    // create new node and its folder location
    //****************************************
    let childArr = [];
    let parentNode = {
        "name": nodeName,
        "value": '1',
        "symbol": "circle",
        "itemStyle": {
            color: "#d00695"
        },
    };
    let childNode = {
        "name": nodeFolderDir,
        "value": '2',
        "symbol": "circle",
        "label": {
            width: 100,
            height: 40,
            lineHeight: 40,
            position: 'right',
            verticalAlign: 'middle',
            align: 'center',
            rich: {
                img1: {
                    backgroundColor: {
                        image: iconDir,
                    },
                    height: 40
                }
            },
            formatter: function (param) {
                let res = "";
                res += '{img1|}' + param.name;
                return res;
            }
        }
    }
    childArr.push(childNode);
    parentNode.children = childArr;
    nodesOption[0].children.push(parentNode);
    //****************************************
    // Deliver selected feature name to the backend to rename.
    window.java({
        request: action + ' ' + nodeName + ' ' + nodeFolderDir,
        persistent: false,
        onSuccess: function (response) {
            console.log(response);
        },
        onFailure: function (error_code, error_message) {
            console.log("[Error from Backend]: " + error_message);
        }
    });
    chartData.setOption(options);
}

/**
 *  Rename tree node.
 */
function renameNode() {
    let selName = selElement.data.name;
    let selValue = selElement.value;
    console.log("[selName] " + selName + ", [selValue] " + selValue);

    if (selValue === "0") {
        alert("Root cannot be renamed.");
        return;
    }

    let action = "";
    if(reqHost === "FoMappingDataFromBackend"){
        action = 'RenameFo';
    }
    if(reqHost === "FiMappingDataFromBackend"){
        action = 'RenameFi';
    }

    let nodeName=prompt("Enter a new name please.");
    if ( nodeName === null || nodeName === "") return;

    let options = chartData.getOption();
    let nodesOption = options.series[0].data;

    for (let m of nodesOption[0].children) {
        if (m.name === selName) {
            m.name = nodeName; // rename the selected node from children list.
            break;
        }
    }
    // Deliver selected feature name to the backend to rename.
    window.java({
        request: action + ' ' + selName + ' ' + nodeName,
        persistent: false,
        onSuccess: function (response) {
            console.log(response);
        },
        onFailure: function (error_code, error_message) {
            console.log("[Error from Backend]: " + error_message);
        }
    });
    chartData.setOption(options);
}


/**
 *  Delete tree node.
 */
function deleteNode() {
    let res = confirm("Delete this node?");
    if (res) {
        let selName = selElement.data.name;
        let selValue = selElement.value;
        console.log("[selName] " + selName + ", [selValue] " + selValue);

        if (selValue === "0") {
            alert("Root cannot be deleted.");
            return;
        }

        let action = "";
        if(reqHost === "FoMappingDataFromBackend"){
            action = 'DeleteFo';
        }
        if(reqHost === "FiMappingDataFromBackend"){
            action = 'DeleteFi';
        }

        let options = chartData.getOption();
        let nodesOption = options.series[0].data;
        let count = 0; // index counter

        for (let m of nodesOption[0].children) {
            console.log("in loop, type " + typeof m + " , " + JSON.stringify(m.name));
            if (m.name === selName) {
                nodesOption[0].children.splice(count, 1)// remove the selected node from children list.
                break;
            }
            count++;
        }
        // Deliver selected feature name to the backend to delete.
        window.java({
            request: action + ' ' + selName,
            persistent: false,
            onSuccess: function (response) {
                console.log(response);
            },
            onFailure: function (error_code, error_message) {
                console.log("[Error from Backend]: " + error_message);
            }
        });
        chartData.setOption(options);
    }
}

/**
 * Create and configure echarts instance.
 * @param DOMName
 * @param JSONData
 */
function createEcharts(DOMName, JSONData) {
    // initialize an instance of echarts
    let myChart = echarts.init(document.getElementById(DOMName));
    let titleText = "";
    if(reqHost === "FoMappingDataFromBackend"){
        titleText = 'Feature-to-Folder';
    }
    if(reqHost === "FiMappingDataFromBackend"){
        titleText = 'Feature-to-File';
    }
    let option = {
        tooltip: {
            trigger: 'item',
            triggerOn: 'mousemove'
        },
        title: {
            text: titleText,
            textStyle: {
                fontSize: 20,
                color: "#d04606"
            },
        },
        series: [
            {
                type: 'tree',
                id: 0,
                name: 'tree1',
                data: [JSONData],

                top: '10%',
                left: '8%',
                bottom: '22%',
                right: '20%',
                symbolSize: 10, // original: 7
                // roam: "scale",

                edgeShape: 'polyline',// option: 'polyline' or 'curve'
                edgeForkPosition: '63%',
                initialTreeDepth: 3,

                lineStyle: {
                    width: 2
                },

                label: {
                    backgroundColor: '#fff',
                    position: 'left',
                    verticalAlign: 'middle',
                    align: 'right',
                    fontSize: 15
                },

                leaves: {
                    label: {
                        position: 'right',
                        verticalAlign: 'middle',
                        align: 'left'
                    }
                },

                expandAndCollapse: true,
                animationDuration: 550,
                animationDurationUpdate: 750
            }
        ]
    };
    let opts = {
        type: "png", // option: png, jpeg
        pixelRatio: 1,// default value: 1
        // backgroundColor: '#fff'
    }
    myChart.setOption(option);
    chartData = myChart;

    myChart.on('contextmenu', function (param) { // options: 'click' / 'dblclick' / 'contextmenu'
        selElement = param;
        document.getElementById(DOMName).oncontextmenu = function () {
            return false; // disable the default right-click menu
        };
        document.onclick = hideMenu;
        rightClick(param);
    });
    setTimeout(function () {
        resBase64 = myChart.getDataURL(opts);
    }, 500);
}
