package org.easelab.hans.toolWindow;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import org.cef.callback.CefCallback;
import org.cef.handler.CefLoadHandler;
import org.cef.handler.CefResourceHandler;
import org.cef.misc.IntRef;
import org.cef.misc.StringRef;
import org.cef.network.CefRequest;
import org.cef.network.CefResponse;

import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.Arrays;

/**
 * A class handling CEF resources
 */
class CustomResourceHandler implements CefResourceHandler {
    private ResourceHandlerState state = new ClosedConnection();

    /**
     * Processes a request for a resource
     * @param cefRequest Request to be processed
     * @param cefCallback Callback that should be continued after this function
     * @return true if the request was processed successfully, else false
     */
    @Override
    public boolean processRequest(CefRequest cefRequest, CefCallback cefCallback) {
        String urlOption = cefRequest.getURL();

        if (urlOption != null) {
            String pathToResource = urlOption.replace("http://myapp", "webview/");
            URL newUrl = getClass().getClassLoader().getResource(pathToResource);
            try {
                state = new OpenedConnection((JarURLConnection) newUrl.openConnection());
            } catch (IOException e) {
                e.printStackTrace();
            }
            cefCallback.Continue();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void getResponseHeaders(CefResponse cefResponse, IntRef responseLength, StringRef redirectUrl) {
        state.getResponseHeaders(cefResponse, responseLength, redirectUrl);
    }

    @Override
    public boolean readResponse(byte[] dataOut, int designedBytesToRead, IntRef bytesRead, CefCallback callback) {
        try {
            return state.readResponse(dataOut, designedBytesToRead, bytesRead, callback);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Closes any open connection the resource handler has.
     */
    @Override
    public void cancel() {
        try {
            state.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        state = new ClosedConnection();
    }

    /**
     * Return the name of current project
     */
    public static String getCurrentProjectName(){
        String name = "";
        try{
            ProjectManager projectManager = ProjectManager.getInstance();
            Project[] openProjects = projectManager.getOpenProjects();
            name = openProjects[0].getName();
        }catch(Exception e){}
        return name;
    }
}

/**
 * Interface with methods needed in classes representing resource handlers
 */
interface ResourceHandlerState {
    default void getResponseHeaders(CefResponse cefResponse, IntRef responseLength, StringRef redirectUrl) { }
    default boolean readResponse(byte[] dataOut, int designedBytesToRead, IntRef bytesRead, CefCallback callback) throws IOException { return false;}
    default void close() throws IOException { }
}

/**
 * A class representing an open connection to a resource
 */
class OpenedConnection implements ResourceHandlerState {

    private final InputStream inputStream;
    private final JarURLConnection connection;

    public OpenedConnection(JarURLConnection connection) throws IOException {
        inputStream = connection.getInputStream();
        this.connection = connection;
    }

    @Override
    public void getResponseHeaders(CefResponse cefResponse, IntRef responseLength, StringRef redirectUrl) {
        try {
            String url = connection.getURL().toString();
            if (url.contains("css")) {
                cefResponse.setMimeType("text/css");
            } else if (url.contains("js")) {
                cefResponse.setMimeType("text/javascript");
            } else {
                cefResponse.setMimeType(connection.getContentType());
            }
            responseLength.set(inputStream.available());
            cefResponse.setStatus(200);
        } catch (IOException e) {
            cefResponse.setError(CefLoadHandler.ErrorCode.ERR_FILE_NOT_FOUND);
            cefResponse.setStatusText(e.getLocalizedMessage());
            cefResponse.setStatus(404);
        }
    }

    @Override
    public boolean readResponse(byte[] dataOut, int designedBytesToRead, IntRef bytesRead, CefCallback callback) throws IOException {
        int availableSize = inputStream.available();
        if (availableSize > 0) {
            // Calculate how many bytes should be read
            int maxBytesToRead = Math.min(availableSize, designedBytesToRead);
            // Set how many bytes was actually read
            int realNumberOfReadBytes = inputStream.read(dataOut, 0, maxBytesToRead);
            bytesRead.set(realNumberOfReadBytes);
            return true;
        } else {
            // If failure to read, close connection
            inputStream.close();
            return false;
        }
    }

    /**
     * Closes the connection to resource
     */
    @Override
    public void close() throws IOException {
        inputStream.close();
    }
}

/**
 * A class representing a closed connection to a resource
 */
class ClosedConnection implements ResourceHandlerState {

    /**
     * Always responds status: 404 Not Found.
     */
    @Override
    public void getResponseHeaders(CefResponse cefResponse, IntRef responseLength, StringRef redirectUrl) {
        cefResponse.setStatus(404);
    }

    /**
     * A closed connection can't read responses and thus always return false
     * @return False
     */
    @Override
    public boolean readResponse(byte[] dataOut, int designedBytesToRead, IntRef bytesRead, CefCallback callback) {
        return false;
    }
}
