package org.easelab.hans.toolWindow;

import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.callback.CefSchemeHandlerFactory;
import org.cef.handler.CefResourceHandler;
import org.cef.network.CefRequest;

/**
 * A class creating a custom CefResourceHandler
 */
public class CustomSchemeHandlerFactory implements CefSchemeHandlerFactory {

    /**
     * Creates a new custom CefResourceHandler
     * @return A new resource handler
     */
    @Override
    public CefResourceHandler create(CefBrowser cefBrowser, CefFrame cefFrame, String s, CefRequest cefRequest) {
        return new CustomResourceHandler();
    }
}
