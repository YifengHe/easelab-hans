package se.ch.hans.MetricsTable;

/**
 * Service providing the TablerViewer
 */
public class TableViewerWindowService {

    public TableWindow tableWindow;

    /**
     * Constructs the service providing the table view
     */
    public TableViewerWindowService() {
        tableWindow = new TableWindow();
    }
}
