package se.ch.hans.MetricsTable;

import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class TableWindowFactory implements ToolWindowFactory {

    /**
     * Adds the table to the TableWindow content
     * @param project The project open in the IDE
     * @param toolWindow The ToolWindow the table is put on
     */
    @Override
    public void createToolWindowContent(@NotNull Project project, ToolWindow toolWindow) {
        TableWindow tableWindow = ServiceManager.getService(project,
                TableViewerWindowService.class).tableWindow;
        JComponent component = toolWindow.getComponent();
        component.getParent().add(tableWindow.content(), 0);
    }
}
