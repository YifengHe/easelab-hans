// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package se.ch.hans.toolwindow;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.intellij.psi.PsiDirectory;
import se.ch.HAnS.folderAnnotation.psi.FolderAnnotationLpq;

import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility class to convert Map<Object, Object> to JSON files or JSON-formatted Strings.
 */
//&begin[MapToJSON, Foo, Bar]
public final class JSONHandler {

    /**
     * @see #HAnSDirectory Name of directory to put files in.
     * @see #pwd Root directory of Project.
     *
     * Most importantly, used here:
     * @see #createDirectory()
     * @see #createJSONFile(String, String)
     */
    private static String HAnSDirectory = "/.HAnS-vis/";

    private JSONHandler() {}

    /**
     * Method to convert a Map<Object, Object> to JSON file.
     *
     * @param mapping Map to convert to JSON file.
     * @param filename name of the file is should be placed in.
     *
     * Note: Said file will be placed under Project/.HAnS-vis/filename.
     */
    public static void toJSONFile(Map mapping, String filename, String path) {
        String jsonString = toJSONString(mapping);
        createDirectory(path);
        createJSONFile(jsonString, filename, path);
    }

    /**
     * Get method to convert a Map<Object, Object> to a JSON-formatted string.
     *
     * @param mapping Map to convert to JSON-formatted string.
     *
     * @return JSON-formatted string containing the Map keys and attributes.
     */
    public static String toJSONString(Map mapping){
        return FormatJSONString(mapping);
    }

    /**
     * Method to convert a Map<FolderAnnotationLpq, PsiDirectory> to a JSON-formatted string.
     * @param mapping
     * @return
     */
    public static String HAnS2JSONString(Map mapping){
        return FoMapping2JSONString(mapping);
    }

    /**
     * Method to convert a Map<String, Object> to a JSON-formatted string.
     *
     * @param mapping Map to convert to JSON-formatted string.
     *
     * @return JSON-formatted string containing the Map keys and attributes.
     */
    private static String FormatJSONString(Map mapping) {
        String jsonString = "{}";
        ObjectMapper mapper = new ObjectMapper();
        try {
            jsonString = mapper.writeValueAsString(mapping);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonString;
    }

    /**
     * Method to convert a Map<FolderAnnotationLpq, PsiDirectory> to a JSON-formatted string.
     *
     * @param mapping Map to convert to JSON-formatted string.
     *
     * @return JSON-formatted string containing the Map keys and attributes.
     *
     */
    private static String FoMapping2JSONString(Map<FolderAnnotationLpq, PsiDirectory> mapping) {
        Map<String,  ArrayList<String>> stringMap = new HashMap<>();

        mapping.keySet().forEach(
                feature -> {
                    String key =  feature.getText();
                    String data =  mapping.get(feature).getName();
                    System.out.println(("[FoMapping2JSONString] " + key + " : " + data));

                    if (!stringMap.containsKey(key)) {
                        stringMap.put(key, new ArrayList<>());
                    }
                    if (!stringMap.get(key).contains(data)) {
                        stringMap.get(key).add(data);
                    }
                }
        );
        return FormatJSONString(stringMap);
    }

    /**
     * Method for creating the directory where the JSON files will be put. If it doesn't exist create it,
     * otherwise do nothing.
     *
     * Note: Name of directory we want to create is already predefined under HAnSDirectory variable.
     */
    private static void createDirectory(String path) {
        File dir = new File(path.concat(HAnSDirectory));
        if(!dir.exists()){
            dir.mkdirs();
        }
    }

    /**
     * Method for creating the JSON file.
     *
     * @param filename Name of the JSON file being created.
     * @param jsonString JSON-formatted string that will be put into file.
     *
     * Note: Name of directory we want to create is already predefined under HAnSDirectory variable.
     */
    private static void createJSONFile(String jsonString, String filename, String path){
        try (FileWriter writer = new FileWriter(path.concat(HAnSDirectory).concat(filename).concat(".json"))) {
            writer.write(jsonString);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
//&end[MapToJSON]

