// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package se.ch.hans.annotations.folder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public interface iFolderAnnotationUtil {

    static Map<String, ArrayList<ArrayList<String>>> getFolderAnnotations(String path) throws IOException {
        return FolderAnnotationLocation.getFolderAnnotations(path);
    }

    static Map<String, ArrayList<ArrayList<String>>> getFolderTanglings(String path) throws IOException {
        return FolderAnnotationTangling.getFolderTanglings(path);
    }
}
