// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package se.ch.hans.annotations.infile;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public final class InFileAnnotationTangling {
    private InFileAnnotationTangling(){}

    public static Map<String, ArrayList<ArrayList<String>>> getCodeTanglings(String path) throws IOException {
        return getFeatureInFileTanglings(path + "/src");
    }

    private static Map<String, ArrayList<ArrayList<String>>> getFeatureInFileTanglings(String path) throws IOException{
        Map<String, ArrayList<ArrayList<String>>> featureCodeTanglings = new HashMap<>();
        Set<String> filePaths = getAllFileLocations(path);

        for (String filePath : filePaths) {
            File file = new File(filePath);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                if (line.contains("&begin")) {
                    line = line.substring(line.indexOf("&begin"));
                    if (line.contains("[") && line.contains("]")) {
                        line = line.substring(line.indexOf('[') + 1, line.indexOf(']'));
                        line = line.replaceAll("\\s+", "");
                        ArrayList<String> features = new ArrayList<>(Arrays.asList(line.split(",")));
                        if (features.size() > 1) {
                            for (String feature : features) {
                                if (!featureCodeTanglings.containsKey(feature)) {
                                    featureCodeTanglings.put(feature, new ArrayList<>());
                                }
                                for (String tangledFeature : features) {
                                    if (!feature.equals(tangledFeature)) {
                                        boolean featureAlreadyExist = false;

                                        for (ArrayList<String> featureTanglings : featureCodeTanglings.get(feature)) {
                                            if (featureTanglings.get(0).equals(tangledFeature)) {
                                                int tanglingDegree = Integer.parseInt(featureTanglings.get(1));
                                                tanglingDegree++;
                                                featureTanglings.set(1, String.valueOf(tanglingDegree));
                                                featureAlreadyExist = true;
                                            }
                                        }
                                        if (!featureAlreadyExist) {
                                            ArrayList<String> data = new ArrayList<>();
                                            data.add(tangledFeature);
                                            data.add("1");
                                            featureCodeTanglings.get(feature).add(data);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if (line.contains("&line")) {
                    line = line.substring(line.indexOf("&line"));
                    if (line.contains("[") && line.contains("]")) {
                        line = line.substring(line.indexOf('[') + 1, line.indexOf(']'));
                        line = line.replaceAll("\\s+", "");
                        ArrayList<String> features = new ArrayList<>(Arrays.asList(line.split(",")));
                        if (features.size() > 1) {
                            for (String feature : features) {
                                if (!featureCodeTanglings.containsKey(feature)) {
                                    featureCodeTanglings.put(feature, new ArrayList<>());
                                }
                                for (String tangledFeature : features) {
                                    if (!feature.equals(tangledFeature)) {
                                        boolean featureAlreadyExist = false;

                                        for (ArrayList<String> featureTanglings : featureCodeTanglings.get(feature)) {
                                            if (featureTanglings.get(0).equals(tangledFeature)) {
                                                int tanglingDegree = Integer.parseInt(featureTanglings.get(1));
                                                tanglingDegree++;
                                                featureTanglings.set(1, String.valueOf(tanglingDegree));
                                                featureAlreadyExist = true;
                                            }
                                        }
                                        if (!featureAlreadyExist) {
                                            ArrayList<String> data = new ArrayList<>();
                                            data.add(tangledFeature);
                                            data.add("1");
                                            featureCodeTanglings.get(feature).add(data);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return featureCodeTanglings;
    }

    /**
     * Helper method to get where all the .feature-to-folder files are located.
     *
     * @param path - path to project root.
     * @return Set containing the file paths to every .feature-to-folder file.
     */
    private static Set<String> getAllFileLocations(String path) {
        Set<String> filePaths = new HashSet<>();
        File root = new File(path);
        try {

            Collection<File> files = FileUtils.listFiles(root, null, true);

            for (File file : files) {
                if(!file.getName().equals(".feature-to-folder") && !file.getName().equals(".feature-to-file"))
                    filePaths.add(file.getAbsolutePath());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filePaths;
    }
}
