// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package se.ch.hans.annotations.folder;

import org.apache.commons.io.FileUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 *  Utility-class to extract folder annotations and their respective mappings from .feature-to-folder files.
 *
 *  Please see *yada yada* on how to declare .feature-to-folder files.
 */
public final class FolderAnnotationLocation {
    private FolderAnnotationLocation() {}
    private static String OS = null;

    /**
     * Method to get folder annotations in root project and all sub-directories.
     *
     * @param path - path to project root directory.
     * @return Map of all the folder annotations in the project and where they are located.
     * @throws IOException -
     * @see #getFeatureFolderMappings(String)
     */
    public static Map<String, ArrayList<ArrayList<String>>> getFolderAnnotations(String path) throws IOException {
        if(isWindows()) {
            return getFeatureFolderMappings(path + "\\src");
        }
        else {
            return getFeatureFolderMappings(path + "/src");
        }
    }

    /**
     * Method to generate feature-to-folder mappings from folder annotations declared in .feature-to-folder files.
     *
     * @param path - Path to project root.
     * @return Map of all the folder annotations in the project and where they are located.
     * @throws IOException -
     * @see #getFeatureFolderLocations(String)
     */
    private static Map<String, ArrayList<ArrayList<String>>> getFeatureFolderMappings(String path) throws IOException {
        Map<String, ArrayList<ArrayList<String>>> featureFolderMappings = new HashMap<>();
        Set<String> featureFolderPaths = getFeatureFolderLocations(path);
        /*
            Get folder annotations from each .feature-to-folder file.
        */
        for (String featureFolderPath : featureFolderPaths) {
            /*
                Get folder the .feature-to-folder file is in as well as all the files in that folder.
             */
            File featureFolderFile = new File(featureFolderPath);
            String parentFolder = featureFolderFile.getParent();
            // Get folder .feature-to-folder is in.
            if(isWindows()) {
                parentFolder = parentFolder.substring(parentFolder.lastIndexOf('\\') + 1);
            } else {
                parentFolder = parentFolder.substring(parentFolder.lastIndexOf('/') + 1);
            }

            File parentFolderFile = featureFolderFile.getParentFile();
            File[] parentFolderFiles = parentFolderFile.listFiles();

            ArrayList<String> data = new ArrayList<>();
            data.add(parentFolder);
            assert parentFolderFiles != null;
            for (File folderFile : parentFolderFiles) {
                String file = folderFile.toString();
                if(isWindows()) {
                    file = file.substring(file.lastIndexOf('\\') + 1);
                } else {
                    file = file.substring(file.lastIndexOf('/') + 1);
                }
                data.add(file);
            }

            /*
                Read the file to extract the feature annotated in the file.
             */
            FileReader fileReader = new FileReader(featureFolderFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                String[] features = line.split(" ");
                for (var feature : features) {
                    if (!featureFolderMappings.containsKey(feature)) {
                        featureFolderMappings.put(feature, new ArrayList<>());
                    }
                    if (!featureFolderMappings.get(feature).contains(data)) {
                        featureFolderMappings.get(feature).add(data);
                    }
                }
            }
        }
        return featureFolderMappings;
    }

    /**
     * Helper method to get where all the .feature-to-folder files are located.
     *
     * @param path - path to project root.
     * @return Set containing the file paths to every .feature-to-folder file.
     */
    private static Set<String> getFeatureFolderLocations(String path) {
        Set<String> featureFolderPaths = new HashSet<>();
        File root = new File(path);
        String filename = ".feature-to-folder";
        try {

            Collection<File> files = FileUtils.listFiles(root, null, true);

            for (File file : files) {
                if (file.getName().equals(filename))
                    featureFolderPaths.add(file.getAbsolutePath());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return featureFolderPaths;
    }

    public static String getOsName()
    {
        if(OS == null) {
            OS = System.getProperty("os.name");
        }
        return OS;
    }
    public static boolean isWindows()
    {
        return getOsName().startsWith("Windows");
    }

}

