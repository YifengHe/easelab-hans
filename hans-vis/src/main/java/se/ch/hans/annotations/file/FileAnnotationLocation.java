// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package se.ch.hans.annotations.file;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public final class FileAnnotationLocation {
    private FileAnnotationLocation(){}
    private static String OS = null;

    /**
     * Method to get folder annotations in root project and all sub-directories.
     *
     * @param path - path to project root directory.
     * @return Map of all the folder annotations in the project and where they are located.
     * @throws IOException -
     * @see #getFeatureFileMappings(String)
     */
    public static Map<String, ArrayList<ArrayList<String>>> getFileAnnotations(String path) throws IOException {
        if(isWindows()){
            return getFeatureFileMappings(path + "\\src");
        } else {
            return getFeatureFileMappings(path + "/src");
        }
    }

    /**
     * Method to generate feature-to-folder mappings from folder annotations declared in .feature-to-folder files.
     *
     * @param path - Path to project root.
     * @return Map of all the folder annotations in the project and where they are located.
     * @throws IOException -
     * @see #getFeatureFileLocations(String)
     */
    private static Map<String, ArrayList<ArrayList<String>>> getFeatureFileMappings(String path) throws IOException {
        Map<String, ArrayList<ArrayList<String>>> featureFileMappings = new HashMap<>();
        Set<String> featureFilePath = getFeatureFileLocations(path);
        /*
            Get file annotations from each .feature-to-folder file.
        */
        for (String s : featureFilePath) {
            /*
                Get folder the .feature-to-folder file is in as well as all the files in that folder.
             */
            File featureFolderFile = new File(s);
            String parentFolder = featureFolderFile.getParent();
            // Get folder .feature-to-file is in.
            if(isWindows()){
                parentFolder = parentFolder.substring(parentFolder.lastIndexOf('\\') + 1);
            } else {
                parentFolder = parentFolder.substring(parentFolder.lastIndexOf('/') + 1);
            }

            /*
                Read the file to extract the feature annotated in the file.
             */
            FileReader fileReader = new FileReader(featureFolderFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            /*
                First line holds files, second line holds features mapped to those files.
            */
            int lineCount = 1;

            ArrayList<String> files = new ArrayList<>();

            while ((line = bufferedReader.readLine()) != null) {
                if (lineCount == 1) {
                    files = new ArrayList<>(Arrays.asList(line.split(" ")));
                    lineCount++;
                } else if (lineCount == 2) {
                    ArrayList<String> features = new ArrayList<>(Arrays.asList(line.replaceAll("\\s","").split(",")));

                    ArrayList<ArrayList<String>> data = new ArrayList<>();
                    for (String file : files) {
                        ArrayList<String> temp = new ArrayList<>();
                        temp.add(file);
                        temp.add(parentFolder);
                        data.add(temp);
                    }
                    for (String feature : features) {
                        if (!featureFileMappings.containsKey(feature)) {
                            featureFileMappings.put(feature, new ArrayList<>());
                        }
                        if (!featureFileMappings.get(feature).contains(data)) {
                            featureFileMappings.get(feature).addAll(data);
                        }
                    }
                    lineCount++;
                } else if (lineCount == 3) {
                    lineCount = 1;
                }
            }
        }
        return featureFileMappings;
    }

    /**
     * Helper method to get where all the .feature-to-folder files are located.
     *
     * @param path - path to project root.
     * @return Set containing the file paths to every .feature-to-folder file.
     */
    private static Set<String> getFeatureFileLocations(String path) {
        Set<String> featureFilePaths = new HashSet<>();
        File root = new File(path);
        String filename = ".feature-to-file";
        try {
            Collection<File> files = FileUtils.listFiles(root, null, true);

            for (File file : files) {
                if (file.getName().equals(filename))
                    featureFilePaths.add(file.getAbsolutePath());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return featureFilePaths;
    }

    public static String getOsName()
    {
        if(OS == null) {
            OS = System.getProperty("os.name");
        }
        return OS;
    }
    public static boolean isWindows()
    {
        return getOsName().startsWith("Windows");
    }


}
