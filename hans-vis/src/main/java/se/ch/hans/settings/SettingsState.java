// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package se.ch.hans.settings;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.util.xmlb.XmlSerializerUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import se.ch.hans.misc.Feature;

/**
 * Stores all values used in the settings page.
 * All values are stored in HAnS-Vis.xml.
 */
@State(
        name = "se.ch.hans.settings.SettingsState",
        storages = {@Storage("HAnS-Vis.xml")}
)
public class SettingsState implements PersistentStateComponent<SettingsState> {
    /** Option chosen for Scattering Degree */
    public int sd = 0;
    /** Option chosen for Number Of Code Annotations */
    public int noca = 0;
    /** Option chosen for Number Of File Annotations */
    public int nofia = 0;
    /** Option chosen for Number Of Folder Annotations */
    public int nofoa = 0;
    /** Option chosen for Tangling Degree */
    public int td = 0;
    /** Option chosen for Lines Of Feature Code */
    public int lofc = 0;

    /** The order in which SD metric should appear in metric view */
    public int sdOrder = Feature.Metric.SD.ordinal();
    /** The order in which NoCA metric should appear in metric view */
    public int nocaOrder = Feature.Metric.NOCA.ordinal();
    /** The order in which NoFiA metric should appear in metric view */
    public int nofiaOrder = Feature.Metric.NOFIA.ordinal();
    /** The order in which NoFoA metric should appear in metric view */
    public int nofoaOrder = Feature.Metric.NOFOA.ordinal();
    /** The order in which TD metric should appear in metric view */
    public int tdOrder = Feature.Metric.TD.ordinal();
    /** The order in which LoFC metric should appear in metric view */
    public int lofcOrder = Feature.Metric.LOFC.ordinal();

    /**
     * Gets an instance of the class from the ServiceManager
     * @return The SettingsState object provided by ServiceManager
     */
    public static SettingsState getInstance() {
        return ServiceManager.getService(SettingsState.class);
    }

    /**
     * Gets the current state
     *
     * @return The current state
     */
    @Nullable
    @Override
    public SettingsState getState() {
        return this;
    }

    /**
     * Loads the state from configuration file (HAnS-Vis.xml).
     *
     * @param state The state object values should be loaded into.
     */
    @Override
    public void loadState(@NotNull SettingsState state) {
        XmlSerializerUtil.copyBean(state, this);
    }
}
