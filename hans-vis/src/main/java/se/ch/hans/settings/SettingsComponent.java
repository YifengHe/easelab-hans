// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package se.ch.hans.settings;

import com.intellij.openapi.ui.ComboBox;
import com.intellij.ui.JBColor;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.table.JBTable;
import com.intellij.util.ui.FormBuilder;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import se.ch.hans.misc.Feature;

import javax.activation.ActivationDataFlavor;
import javax.activation.DataHandler;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DragSource;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

/**
 * A class providing the settings page component
 */
public class SettingsComponent {
    /** A metrics index in table */
    private final int[] metricIndexes = { Feature.Metric.SD.ordinal(),    Feature.Metric.NOCA.ordinal(),
                                          Feature.Metric.NOFIA.ordinal(), Feature.Metric.NOFOA.ordinal(),
                                          Feature.Metric.TD.ordinal(),    Feature.Metric.LOFC.ordinal() };

    /** Text for option 0 in comboBox */
    @Nls(capitalization = Nls.Capitalization.Sentence)
    private final String showOption0 = "Both";
    /** Text for option 1 in comboBox */
    @Nls(capitalization = Nls.Capitalization.Sentence)
    private final String showOption1 = "Only metrics view";
    /** Text for option 2 in comboBox */
    @Nls(capitalization = Nls.Capitalization.Sentence)
    private final String showOption2 = "Only miniature view";
    /** Text for option 3 in comboBox */
    @Nls(capitalization = Nls.Capitalization.Sentence)
    private final String showOption3 = "<none>";

    // UI elements
    private final JPanel mainPanel;
    private final JBTable table;

    /**
     * Constructs the settings page for the plugin
     */
    public SettingsComponent() {
        // Setup a TableModel
        String[] columns = {"Metric", "Show"};
        String[][] data = {{"SD",showOption0}, {"NoCA",showOption0}, {"NoFiA",showOption0},
                           {"NoFoA",showOption0}, {"TD",showOption0}, {"LoFC",showOption0}};
        DefaultTableModel model = new DefaultTableModel(data, columns) {
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return columnIndex != 0;
            }
        };

        // Setup the table
        table = new JBTable(model);
        table.getTableHeader().setReorderingAllowed(false);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        // Setup support for dragging
        table.setDragEnabled(true);
        table.setDropMode(DropMode.INSERT_ROWS);
        table.setTransferHandler(new TableDragTransferHandler(table, this));
        table.addMouseMotionListener(new MouseMotionListener() {
            public void mouseDragged(MouseEvent e) {
                e.consume();
                JComponent c = (JComponent) e.getSource();
                TransferHandler handler = c.getTransferHandler();
                handler.exportAsDrag(c, e, TransferHandler.MOVE);
            }

            public void mouseMoved(MouseEvent e) {
            }
        });

        // Make second column have ComboBox options
        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.addItem(showOption0);
        comboBox.addItem(showOption1);
        comboBox.addItem(showOption2);
        comboBox.addItem(showOption3);
        TableColumn secCol = table.getColumnModel().getColumn(1);
        secCol.setCellEditor(new DefaultCellEditor(comboBox));

        JBLabel tableExplanation = new JBLabel("Move metrics up and down to change order they appear in the metrics view");
        tableExplanation.setForeground(JBColor.foreground().darker());

        // Create the main panel
        mainPanel = FormBuilder.createFormBuilder()
                .addComponent(new JBLabel("Metric view:"))
                .addComponent(table.getTableHeader())
                .addComponent(table)
                .addComponent(tableExplanation)
                .addComponentFillVertically(new JPanel(), 0).getPanel();
    }

    /**
     * Gets the main JPanel of the settings page
     * @return A JPanel containing the settings page content
     */
    public JPanel getPanel() {
        return mainPanel;
    }

    /**
     * Gets the option chosen for a specific metric
     * @param metric The metric whose option should be returned
     * @return The option chosen (as an ID)
     */
    public int getMetricOption(@NotNull Feature.Metric metric) {
        return getOptionID((String) table.getValueAt(getMetricOrder(metric), 1));
    }

    /**
     * Sets the option for a specific metric
     * @param option The option to be set (as an ID)
     * @param metric The metric whose option is to be set
     */
    public void setMetricOption(int option, @NotNull Feature.Metric metric) {
        table.setValueAt(getOptionString(option), getMetricOrder(metric), 1);
    }

    /**
     * Gets the order the metric should be shown in
     * @param metric The metric whose order should be returned
     * @return The order it should be shown (starting from 0)
     */
    private int getMetricOrder(int metric) {
        if (metric >= 0 && metric <= metricIndexes.length) {
            return metricIndexes[metric];
        }
        return -1;
    }

    /**
     * Gets the order the metric should be show in
     * @param metric The metric whose order should be returned
     * @return The order it should be shown (starting from 0)
     */
    public int getMetricOrder(@NotNull Feature.Metric metric) {
        return getMetricOrder(metric.ordinal());
    }

    /**
     * Sets the order a metric should be shown in
     * @param order The order it should be set to (starting from 0)
     * @param rowOrMetric The metric (or row) whose order should be set
     * @param isRow Boolean saying whether or nor rowOrMetric is a row
     */
    protected void setMetricOrder(int order, int rowOrMetric, boolean isRow) {
        // Move row to target row
        int rowFrom = !isRow ? getMetricOrder(rowOrMetric) : rowOrMetric;
        ((DefaultTableModel) table.getModel()).moveRow(rowFrom, rowFrom, order);

        // Switch indexes
        boolean movedUp = rowFrom < order;
        for (int i = 0; i < metricIndexes.length; i++) {
            int metricIndex = getMetricOrder(i);
            int newIndex = movedUp ? (metricIndex - 1) : (metricIndex + 1);
            boolean shouldChange = (order <= metricIndex && metricIndex < rowFrom)
                    || (rowFrom < metricIndex && metricIndex <= order);
            if (metricIndex == rowFrom) {
                metricIndexes[i] = order;
            } else if (shouldChange) {
                metricIndexes[i] = newIndex;
            }
        }
    }

    /**
     * Sets the order the metric should be shown in
     * @param order The order it should be set to (starting from 0)
     * @param metric The metric whose order should be set
     */
    public void setMetricOrder(int order, @NotNull Feature.Metric metric) {
        setMetricOrder(order,metric.ordinal(), false);
    }

    /**
     * Gets the ID of a chosen show option
     * @param optionText The text to be converted to an ID
     * @return The ID optionText should be stored as in settings file
     */
    private int getOptionID(String optionText) {
        int id;
        switch (optionText) {
            case showOption0: id = 0; break;
            case showOption1: id = 1; break;
            case showOption2: id = 2; break;
            case showOption3: id = 3; break;
            default: id = -1; break;
        }
        return id;
    }

    /**
     * Gets the option corresponding to a specific ID
     * @param id The ID of the wanted string
     * @return The string corresponding to id
     */
    private String getOptionString(int id) {
        String str;
        switch (id) {
            case 0: str = showOption0; break;
            case 1: str = showOption1; break;
            case 2: str = showOption2; break;
            case 3: str = showOption3; break;
            default: str = ""; break;
        }
        return str;
    }
}

/**
 * A class handling drag and drop for tables
 */
class TableDragTransferHandler extends TransferHandler {
    private final DataFlavor localObjectFlavor;
    private final JBTable table;
    private final SettingsComponent component;

    public TableDragTransferHandler(JBTable table, SettingsComponent component) {
        localObjectFlavor = new ActivationDataFlavor(Integer.class,
                "application/x-java-Integer;class=java.lang.Integer",
                "Integer Row Index");
        this.table = table;
        this.component = component;
    }

    /**
     * Allows transfer of initially selected row
     * @param c The table in which the dragging happens
     * @return A transferable object containing the selected row in the table
     */
    @Override
    protected Transferable createTransferable(JComponent c) {
        assert (c == table);
        return new DataHandler(table.getSelectedRow(), localObjectFlavor.getMimeType());
    }

    /**
     * Returns whether or not a certain action can be performed
     * (Only allows drop action)
     * @param support Information about action
     * @return True if possible, else not
     */
    @Override
    public boolean canImport(TransferSupport support) {
        boolean b = support.isDrop() && support.getComponent() == table
                && support.isDataFlavorSupported(localObjectFlavor);
        table.setCursor(b ? DragSource.DefaultMoveDrop : DragSource.DefaultMoveNoDrop);
        return b;
    }

    @Override
    public int getSourceActions(JComponent c) {
        return TransferHandler.COPY_OR_MOVE;
    }

    /**
     * Tries to perform a drag and drop action
     * @param support Information about the action
     * @return Whether the action was successfully performed
     */
    @Override
    public boolean importData(TransferSupport support) {
        // Make sure action can be done
        if (!canImport(support)) {
            return false;
        }

        try {
            // Get target row
            JTable.DropLocation dl = (JTable.DropLocation) support.getDropLocation();
            int targetRow = dl.getRow();
            int max = table.getModel().getRowCount() - 1;
            if (targetRow < 0 || targetRow > max) targetRow = max;

            support.getComponent().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

            // Move row to target row
            int rowFrom = (Integer) support.getTransferable().getTransferData(localObjectFlavor);
            component.setMetricOrder(targetRow, rowFrom, true);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    protected void exportDone(JComponent c, Transferable t, int act) {
        if ((act == TransferHandler.MOVE) || (act == TransferHandler.NONE)) {
            table.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }
}