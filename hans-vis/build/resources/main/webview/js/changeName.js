// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Functions for changing vis.js node names

function changeNodes(nodes, oldname, newname) {
    nodes.get(oldname).id = newname
    nodes.get(oldname).label = newname
}

function changeEdges(edgeArray,node,newname) {
    var connectedEdges = edgeArray.get(network.getConnectedEdges(node))
    for ( var i = 0; i < connectedEdges.length; i++) {
        if (edgeArray.get(connectedEdges[i].id).from == node){
            edgeArray.updateOnly({id: edgeArray.get(connectedEdges[i].id).id, from: newname })
        } else if (edgeArray.get(connectedEdges[i].id).to == node) {
            edgeArray.updateOnly({id: edgeArray.get(connectedEdges[i].id).id, to: newname})
        }
    }
}

function updateChanges() {

    nodes = new vis.DataSet(nodes.get())

    network.setData({nodes: nodes, edges: edges});

}